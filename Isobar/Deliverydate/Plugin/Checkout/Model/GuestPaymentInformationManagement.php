<?php
namespace Isobar\Deliverydate\Plugin\Checkout\Model;


class GuestPaymentInformationManagement
{
    /**
     * @var \Isobar\Deliverydate\Helper\Data
     */
    protected $deliveryHelper;

    /**
     * GuestPaymentInformationManagement constructor.
     * @param \Isobar\Deliverydate\Helper\Data $deliveryHelper
     */
    public function __construct(
        \Isobar\Deliverydate\Helper\Data $deliveryHelper
    ) {
        $this->deliveryHelper = $deliveryHelper;
    }

    /**
     * Set payment information and place order for a specified cart.
     *
     * @param int $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return int Order ID.
     */
    public function aroundSavePaymentInformationAndPlaceOrder(
        $subject,
        callable $proceed,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null)
    {
        $result = $proceed($cartId, $paymentMethod, $billingAddress);
        $additionalData = $paymentMethod->getAdditionalData();
        $this->deliveryHelper->saveDeliveryAfterPlaceOrder($result, $additionalData);
        return $result;
    }
}
