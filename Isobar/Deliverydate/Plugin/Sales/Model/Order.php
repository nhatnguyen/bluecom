<?php

namespace Isobar\Deliverydate\Plugin\Sales\Model;

class Order
{
    /**
     * @var \Isobar\Deliverydate\Api\DeliveryRepositoryInterface
     */
    protected $deliveryRepository;

    /**
     * @var \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory
     */
    protected $deliveryFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderExtensionFactory
     */
    protected $orderExtensionFactory;

    /**
     * @var \Isobar\Deliverydate\Helper\Data
     */
    protected $helper;

    /**
     * Order constructor.
     * @param \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository
     * @param \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory $deliveryFactory
     * @param \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory
     * @param \Isobar\Deliverydate\Helper\Data $helper
     */
    public function __construct(
        \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository,
        \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory $deliveryFactory,
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \Isobar\Deliverydate\Helper\Data $helper

    ) {
        $this->deliveryRepository = $deliveryRepository;
        $this->deliveryFactory = $deliveryFactory;
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->helper = $helper;
    }

    /**
     * After load object data
     * @param $subject
     * @param $result
     * @return mixed
     */
    public function afterLoad($subject, $result)
    {
        try {
            $delivery = $this->deliveryRepository->getByOrderId($result->getId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return $result;
        }

        $orderExtension = $result->getExtensionAttributes();
        if ($orderExtension === null) {
            $orderExtension = $this->orderExtensionFactory->create();
        }
        $orderExtension->setShippingDelivery($delivery);
        $result->setExtensionAttributes($orderExtension);
        return $result;
    }
}
