<?php

namespace Isobar\Deliverydate\Plugin\Sales\Model;

class OrderRepository
{
    /**
     * @var \Isobar\Deliverydate\Api\DeliveryRepositoryInterface
     */
    protected $deliveryRepository;

    /**
     * @var \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory
     */
    protected $deliveryFactory;

    /**
     * @var \Magento\Sales\Api\Data\OrderExtensionFactory
     */
    protected $orderExtensionFactory;

    /**
     * @var \Isobar\Deliverydate\Helper\Data
     */
    protected $helper;



    public function __construct(
        \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository,
        \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory $deliveryFactory,
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \Isobar\Deliverydate\Helper\Data $helper

    ) {
        $this->deliveryRepository = $deliveryRepository;
        $this->deliveryFactory = $deliveryFactory;
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->helper = $helper;
    }

    /**
     * After load entity
     *
     * @param int $id
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundGet($subject, callable $proceed, $id)
    {
        $result = $proceed($id);
        try {
            $delivery = $this->deliveryRepository->getByOrderId($result->getId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return $result;
        }

        $orderExtension = $result->getExtensionAttributes();
        if ($orderExtension === null) {
            $orderExtension = $this->orderExtensionFactory->create();
        }
        $orderExtension->setShippingDelivery($delivery);
        $result->setExtensionAttributes($orderExtension);
        return $result;
    }
}
