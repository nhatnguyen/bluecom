<?php
namespace Isobar\Deliverydate\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Isobar\Deliverydate\Api\DeliveryRepositoryInterface
     */
    protected $deliveryRepository;

    /**
     * @var \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory
     */
    protected $deliveryFactory;

    /**
     * @var \Isobar\Deliverydate\Block\Adminhtml\Delivery
     */
    protected $deliveryBlock;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $storeTime;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Data constructor.
     * @param \Isobar\Deliverydate\Block\Adminhtml\Delivery $deliveryBlock
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository
     * @param \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory $deliveryFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Isobar\Deliverydate\Block\Adminhtml\Delivery $deliveryBlock,
        \Magento\Framework\App\Request\Http $request,
        \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository,
        \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory $deliveryFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->deliveryRepository = $deliveryRepository;
        $this->deliveryFactory = $deliveryFactory;
        $this->deliveryBlock = $deliveryBlock;
        $this->request = $request;
        $this->storeTime = $timezone;
        $this->storeManager = $storeManager;
    }

    /**
     * Get HTML from delivery info
     * @param $order
     * @return string
     */
    public function getShippingDeliveryHtml($order)
    {
        $delivery = $order->getExtensionAttributes()->getShippingDelivery();
        if ($delivery && $delivery->getOrderId() === $order->getId()) {
            $deliveryDateBlock = $this->deliveryBlock;
            $deliveryDateBlock->setDeliveryDate($delivery->getDeliveryDate());
            $deliveryDateBlock->setDeliveryComment($delivery->getDeliveryComment());
            $deliveryDateBlock->setTemplate('Isobar_Deliverydate::sales/order/info/delivery.phtml');
            return $deliveryDateBlock->toHtml();
        }
        return '';
    }

    /**
     * Get request page name same as handle
     * @return string
     */
    public function getPageName()
    {
        $pageName = $this->request->getModuleName(). '_' . $this->request->getControllerName() . '_' .$this->request->getActionName();
        return $pageName;
    }

    /**
     * Save delivery after checkout placeorder
     * @param $orderId
     * @param $additionalData
     */
    public function saveDeliveryAfterPlaceOrder($orderId, $additionalData)
    {
        $delivery = $this->deliveryFactory->create();
        if (isset($additionalData['delivery_date']) && isset($additionalData['delivery_comment'])) {
            $deliveryTimestamp = $this->getDeliveryDateTimestampByTimezone($additionalData['delivery_date'] . ' 00:00:00');
            $delivery->setOrderId($orderId)
                ->setDeliveryDate($deliveryTimestamp)
                ->setDeliveryComment($additionalData['delivery_comment'])
                ->setUpdatedAt((new \DateTime())->getTimestamp())
                ->setCreatedAt((new \DateTime())->getTimestamp());
            $this->deliveryRepository->save($delivery);
        }
    }

    /**
     * Get timestamp after convert from store timezone config to UTC
     * @param $deliverydate
     * @param string $fromTz
     * @param string $toTz
     * @return int
     */
    public function getDeliveryDateTimestampByTimezone($deliverydate, $fromTz='UTC', $toTz='UTC')
    {
        $fromTz = $this->storeTime->getConfigTimezone('store', $this->storeManager->getStore());
        $date = new \DateTime($deliverydate, new \DateTimeZone($fromTz));
        $date->setTimezone(new \DateTimeZone($toTz));
        return $date->getTimestamp();
    }
}
