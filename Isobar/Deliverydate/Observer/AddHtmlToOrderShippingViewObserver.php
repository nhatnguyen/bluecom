<?php
namespace Isobar\Deliverydate\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddHtmlToOrderShippingViewObserver implements ObserverInterface
{
    /**
     * @var \Isobar\Deliverydate\Helper\Data
     */
    protected $helper;

    /**
     * AddHtmlToOrderShippingViewObserver constructor.
     * @param \Isobar\Deliverydate\Helper\Data $helper
     */
    public function __construct(\Isobar\Deliverydate\Helper\Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        if($observer->getElementName() == 'order_shipping_view') {
            $orderShippingViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderShippingViewBlock->getOrder();
            $html = $this->helper->getShippingDeliveryHtml($order);
            $html = $observer->getTransport()->getOutput() . $html;
            $observer->getTransport()->setOutput($html);
        }
    }
}
