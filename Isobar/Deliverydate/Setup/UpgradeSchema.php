<?php

namespace Isobar\Deliverydate\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();


        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('shipping_delivery_date'),
                'delivery_date',
                'delivery_date',
                ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP]
            );
        }

        $setup->endSetup();
    }
}