var config = {
    "map": {
        "*": {
            //for overriding
            'Magento_Checkout/js/model/shipping-save-processor/default': 'Isobar_Deliverydate/js/checkout/model/shipping-save-processor/default',
            'Magento_Checkout/js/action/place-order': 'Isobar_Deliverydate/js/checkout/action/place-order',
            'Magento_Checkout/js/view/shipping': 'Isobar_Deliverydate/js/checkout/view/shipping'
        }
    }
};