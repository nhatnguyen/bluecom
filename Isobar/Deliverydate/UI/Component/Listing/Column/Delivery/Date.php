<?php
namespace Isobar\Deliverydate\UI\Component\Listing\Column\Delivery;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Date
 * @package Isobar\Deliverydate\UI\Component\Listing\Column\Delivery
 */
class Date extends Column
{
    /**
     * @var \Isobar\Deliverydate\Api\DeliveryRepositoryInterface
     */
    protected $deliveryRepository;

    /**
     * @var \Isobar\Deliverydate\Block\Adminhtml\Delivery
     */
    protected $eleTemplate;

    /**
     * Date constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository
     * @param \Isobar\Deliverydate\Block\Adminhtml\Delivery $elementTemplate
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository,
        \Isobar\Deliverydate\Block\Adminhtml\Delivery $elementTemplate,
        array $components = [],
        array $data = []
    ) {
        $this->eleTemplate = $elementTemplate;
        $this->deliveryRepository = $deliveryRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        $jsConfig = $this->getData('js_config');
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($jsConfig['extends'])) {
                    if ('sales_order_grid' === $jsConfig['extends']) {
                        $orderId = $item['entity_id'];
                    } else {
                        $orderId = $item['order_id'];
                    }
                    if (0 < $orderId) {
                        $delivery = $this->deliveryRepository->getByOrderId($orderId);
                        if ($delivery->getDeliveryDate()) {
                            $deliveryDate = $this->eleTemplate->formatDate(
                                $this->eleTemplate->getDeliveryAdminDate($delivery->getDeliveryDate()),
                                \IntlDateFormatter::LONG,
                                true
                            );
                            $item[$this->getData('name')] = $deliveryDate;
                        }

                    }
                }
            }
        }
        return $dataSource;
    }
}
