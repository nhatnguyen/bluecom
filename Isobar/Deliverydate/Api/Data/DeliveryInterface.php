<?php

namespace Isobar\Deliverydate\Api\Data;


interface DeliveryInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     * Constants defined for keys of data array
     */
    const ORDER_ID              = 'order_id';
    const DELIVERY_DATE         = 'delivery_date';
    const DELIVERY_COMMENT      = 'delivery_comment';
    const CREATED_AT            = 'created_at';
    const UPDATED_AT            = 'updated_at';

    /**
     * Get delivery id
     * @return int|null
     */
    public function getId();

    /**
     * Set delivery id
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get delivery order_id
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set delivery order_id
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Get delivery date
     * @return string|null
     */
    public function getDeliveryDate();

    /**
     * Set delivery date
     * @param string $deliveryDate
     * @return $this
     */
    public function setDeliveryDate($deliveryDate);

    /**
     * Get delivery comment
     * @return string|null
     */
    public function getDeliveryComment();

    /**
     * Set delivery comment
     * @param string $deliveryComment
     * @return $this
     */
    public function setDeliveryComment($deliveryComment);

    /**
     * Get delivery created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set delivery created date
     *
     * @param string $timeStamp
     * @return $this
     */
    public function setCreatedAt($timeStamp);

    /**
     * Get delivery created date
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set delivery updated date
     *
     * @param string $timeStamp
     * @return $this
     */
    public function setUpdatedAt($timeStamp);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Isobar\Deliverydate\Api\Data\DeliveryExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Isobar\Deliverydate\Api\Data\DeliveryExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Isobar\Deliverydate\Api\Data\DeliveryExtensionInterface $extensionAttributes);
}
