<?php

namespace Isobar\Deliverydate\Api\Data;

/**
 * @api
 */
interface DeliverySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Isobar\Deliverydate\Api\Data\DeliveryInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Isobar\Deliverydate\Api\Data\DeliveryInterface[] $deliveries
     * @return $this
     */
    public function setItems(array $deliveries);
}
