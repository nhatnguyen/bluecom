<?php

namespace Isobar\Deliverydate\Api;


interface DeliveryRepositoryInterface
{
    /**
     * @param \Isobar\Deliverydate\Api\Data\DeliveryInterface $delivery
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Isobar\Deliverydate\Api\Data\DeliveryInterface $delivery);

    /**
     * Get info about delivery by id
     *
     * @param int $modelId
     * @return \Isobar\Deliverydate\Api\Data\DeliveryInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($modelId);

    /**
     * Get info about delivery by order id.
     *
     * @param int $orderId
     * @return \Isobar\Deliverydate\Api\Data\DeliveryInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByOrderId($orderId);

    /**
     * Delete delivery
     *
     * @param \Isobar\Deliverydate\Api\Data\DeliveryInterface $delivery
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Isobar\Deliverydate\Api\Data\DeliveryInterface $delivery);

    /**
     * Delete delivery by id
     *
     * @param int $id
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * Get delivery list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Isobar\Deliverydate\Api\Data\DeliverySearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
