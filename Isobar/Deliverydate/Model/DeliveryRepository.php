<?php

namespace Isobar\Deliverydate\Model;

class DeliveryRepository implements \Isobar\Deliverydate\Api\DeliveryRepositoryInterface
{
    /**
     * @var \Isobar\Deliverydate\Model\DeliveryFactory
     */
    protected $modelFactory;

    /**
     * @var \Isobar\Deliverydate\Model\ResourceModel\Delivery
     */
    protected $resourceModel;

    /**
     * @var \Isobar\Deliverydate\Model\ResourceModel\Delivery\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Isobar\Deliverydate\Api\Data\DeliverySearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * DeliveryRepository constructor.
     * @param \Isobar\Deliverydate\Model\DeliveryFactory $modelFactory
     * @param \Isobar\Deliverydate\Model\ResourceModel\Delivery $resourceModel
     * @param \Isobar\Deliverydate\Model\ResourceModel\Delivery\CollectionFactory $collectionFactory
     * @param \Isobar\Deliverydate\Api\Data\DeliverySearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Isobar\Deliverydate\Model\DeliveryFactory $modelFactory,
        \Isobar\Deliverydate\Model\ResourceModel\Delivery $resourceModel,
        \Isobar\Deliverydate\Model\ResourceModel\Delivery\CollectionFactory $collectionFactory,
        \Isobar\Deliverydate\Api\Data\DeliverySearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->modelFactory = $modelFactory;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Isobar\Deliverydate\Api\Data\DeliveryInterface $model) {
        try {
            $this->resourceModel->save($model);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save item'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($modelId) {
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $modelId);
        if(!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested item doesn\'t exist'));
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function getByOrderId($orderId) {
        $model = $this->modelFactory->create();
        $modelId = $this->resourceModel->getIdByOrderId($orderId);
        if (!$modelId) {
            return $model;
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested item doesn\'t exist'));
        }
        $this->resourceModel->load($model, $modelId);
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Isobar\Deliverydate\Api\Data\DeliveryInterface $model) {
        $id = $model->getId();
        try {
            $this->resourceModel->delete($model);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(__('Unable to remove item %1', $id));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($modelId) {
        $model = $this->get($modelId);
        return $this->delete($model);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria) {
        /** @var \Isobar\Deliverydate\Model\ResourceModel\Delivery\Collection $collection */
        $collection = $this->collectionFactory->create();

        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
            );
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->load();

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Isobar\Deliverydate\Model\ResourceModel\Delivery\Collection $collection
     * @return void
     */
    private function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Isobar\Deliverydate\Model\ResourceModel\Delivery\Collection $collection)
    {
        $fields = [];
        $conditions = [];

        foreach($filterGroup->getFilters() as $filter){
            $field = $filter->getField();
            $condition = $filter->getConditionType() ?: 'eq';
            $value = $filter->getValue();

            $fields[] = $field;
            $conditions[] = [ $condition => $value ];
        }

        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
}