<?php

namespace Isobar\Deliverydate\Model;


class Delivery extends \Magento\Framework\Model\AbstractExtensibleModel implements \Isobar\Deliverydate\Api\Data\DeliveryInterface
{

    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Deliverydate\Model\ResourceModel\Delivery');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId($orderId)
    {
        return $this->setData('order_id', $orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getDeliveryDate()
    {
        return $this->_getData(self::DELIVERY_DATE);
    }

    /**
     * {@inheritdoc}
     */
    public function setDeliveryDate($deliveryDate)
    {
        return $this->setData('delivery_date', $deliveryDate);
    }

    /**
     * {@inheritdoc}
     */
    public function getDeliveryComment()
    {
        return $this->_getData(self::DELIVERY_COMMENT);
    }

    /**
     * {@inheritdoc}
     */
    public function setDeliveryComment($deliveryComment)
    {
        return $this->setData('delivery_comment', $deliveryComment);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt() {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($timeStamp) {
        return $this->setData(self::CREATED_AT, $timeStamp);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt() {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt($timeStamp) {
        return $this->setData(self::UPDATED_AT, $timeStamp);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtensionAttributes() {
        $extensionAttributes = $this->_getExtensionAttributes();
        if (!$extensionAttributes) {
            return $this->extensionAttributesFactory->create('\Isobar\Deliverydate\Api\Data\DeliveryInterface');
        }
        return $extensionAttributes;
    }

    /**
     * {@inheritdoc}
     */
    public function setExtensionAttributes(\Isobar\Deliverydate\Api\Data\DeliveryExtensionInterface $extensionAttributes) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
