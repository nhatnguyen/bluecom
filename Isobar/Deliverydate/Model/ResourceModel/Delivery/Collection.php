<?php

namespace Isobar\Deliverydate\Model\ResourceModel\Delivery;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * _contruct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Deliverydate\Model\Delivery', 'Isobar\Deliverydate\Model\ResourceModel\Delivery');
    }
}
