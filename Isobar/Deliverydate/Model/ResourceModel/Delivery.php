<?php

namespace Isobar\Deliverydate\Model\ResourceModel;


class Delivery extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    const TABLE_NAME = 'shipping_delivery_date';
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, 'id');
    }

    /**
     * Get delivery by order id
     *
     * @param int $orderId
     * @return int|false
     */
    public function getIdByOrderId($orderId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from($this->getTable(self::TABLE_NAME), 'id')->where('order_id = :order_id');

        $bind = [':order_id' => $orderId];

        return $connection->fetchOne($select, $bind);
    }
}
