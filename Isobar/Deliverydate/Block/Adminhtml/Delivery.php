<?php
namespace Isobar\Deliverydate\Block\Adminhtml;

class Delivery extends \Magento\Framework\View\Element\Template
{
    /**
     * Get delivery date for admin store
     * @param $deliveryDate
     * @return \DateTime
     */
    public function getDeliveryAdminDate($deliveryDate)
    {
        return $this->_localeDate->date(new \DateTime($deliveryDate));
    }
}
