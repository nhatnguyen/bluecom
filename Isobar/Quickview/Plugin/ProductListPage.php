<?php

namespace Isobar\Quickview\Plugin;

class ProductListPage
{
    /**
     * @param \Magento\Catalog\Block\Product\ListProduct $subject
     * @param \Closure $proceed
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function aroundGetProductDetailsHtml(
        \Magento\Catalog\Block\Product\ListProduct $subject,
        \Closure $proceed,
        \Magento\Catalog\Model\Product $product
    )
    {
        $result = $proceed($product);
        $buttonText = __('Quick View');
        $result .= '<a class="isobar-quickview" data-id="' . $product->getId() . '" href="javascript:void(0);"><h3>' . $buttonText . '</h3></a>';
        return $result;
    }

}
