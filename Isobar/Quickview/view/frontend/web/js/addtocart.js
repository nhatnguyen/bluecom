require(['jquery'], function($) {
    $('#product_addtocart_form').submit(function(e) {
        //validate here
        e.preventDefault();
        if ($(this).validation('isValid')) {
            var formData = $( this ).serialize();
            var button = $(this).find('button[type="submit"]');
            $(button).addClass('disabled');
            $(button).find('span').text('Adding...');
            $.ajax({
                url: $( this ).attr('action'),
                type: "post",
                data: formData,
                success: function(data) {
                    $(button).removeClass('disabled');
                    $(button).find('span').text('Add to Cart');
                }
            });
        }
    });
});