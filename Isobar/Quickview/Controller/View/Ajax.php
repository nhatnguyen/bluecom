<?php

namespace Isobar\Quickview\Controller\View;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
class Ajax extends \Magento\Catalog\Controller\Product\View
{
    /**
     * Constructor
     *
     * @param Context $context
     * @param \Magento\Catalog\Helper\Product\View $viewHelper
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Isobar\Quickview\Helper\Product\View $viewHelper,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory
    ) {
        $this->viewHelper = $viewHelper;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $viewHelper, $resultForwardFactory, $resultPageFactory);
    }
}