<?php

namespace Isobar\Bannerslider\Api\Data;


interface ItemInterface
{
    /**
     * Constants defined for keys of data array
     */
    const TITLE     = 'title';
    const IMG       = 'img';
    const IMG_LINK  = 'img_link';
    const POSITION  = 'position';
    const STATUS    = 'status';
    const ALT    = 'alt';

    /**
     * Get item id
     * @return int|null
     */
    public function getId();

    /**
     * Set item id
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get item title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set item title
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Get item img
     * @return string|null
     */
    public function getImg();

    /**
     * Set item img
     * @param string $img
     * @return $this
     */
    public function setImg($img);

    /**
     * Get item img_link
     * @return string|null
     */
    public function getImgLink();

    /**
     * Set item img_link
     * @param string $imgLink
     * @return $this
     */
    public function setImgLink($imgLink);

    /**
     * Get item alt
     * @return string
     */
    public function getAlt();

    /**
     * Set item alt
     * @param string $alt
     * @return $this
     */
    public function setAlt($alt);

    /**
     * Get item position
     * @return int|null
     */
    public function getPosition();

    /**
     * Set item position
     * @param int $position
     * @return $this
     */
    public function setPosition($position);

    /**
     * Get item status
     * @return int|null
     */
    public function getStatus();

    /**
     * Set item status
     * @param int $status
     * @return $this
     */
    public function setStatus($status);
}