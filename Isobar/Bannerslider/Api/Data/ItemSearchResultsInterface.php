<?php

namespace Isobar\Bannerslider\Api\Data;

/**
 * @api
 */
interface ItemSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Isobar\Bannerslider\Api\Data\ItemInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Isobar\Bannerslider\Api\Data\ItemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
