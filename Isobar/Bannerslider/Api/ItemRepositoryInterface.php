<?php

namespace Isobar\Bannerslider\Api;


interface ItemRepositoryInterface
{
    /**
     * @param \Isobar\Bannerslider\Api\Data\ItemInterface $item
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Isobar\Bannerslider\Api\Data\ItemInterface $item);

    /**
     * Get info about item by item id
     *
     * @param int $modelId
     * @return \Isobar\Bannerslider\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($modelId);

    /**
     * Retrieve block.
     *
     * @param int $id
     * @return \Isobar\Bannerslider\Api\Data\ItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Delete item
     *
     * @param \Isobar\Bannerslider\Api\Data\ItemInterface $item
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Isobar\Bannerslider\Api\Data\ItemInterface $item);

    /**
     * Delete item by id
     *
     * @param int $itemId
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($itemId);

    /**
     * Get item list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Isobar\Bannerslider\Api\Data\ItemSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}