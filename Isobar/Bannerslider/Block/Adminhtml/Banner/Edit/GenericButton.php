<?php
namespace Isobar\Bannerslider\Block\Adminhtml\Banner\Edit;

use Magento\Backend\Block\Widget\Context;
use Isobar\Bannerslider\Api\ItemRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ItemRepositoryInterface
     */
    protected $itemRepository;

    /**
     * @param Context $context
     * @param ItemRepositoryInterface $itemRepository
     */
    public function __construct(
        Context $context,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->context = $context;
        $this->itemRepository = $itemRepository;
    }

    /**
     * Return Banner ID
     *
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->itemRepository->get(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
