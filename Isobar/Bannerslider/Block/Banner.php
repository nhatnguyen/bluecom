<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 13/06/2017
 * Time: 17:55
 */

namespace Isobar\Bannerslider\Block;


class Banner extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory
     */
    protected $itemFactory;

    /**
     * @var \Isobar\Bannerslider\Api\ItemRepositoryInterface
     */
    protected $itemRepository;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Api\SortOrder
     */

    /**
     * @var \Isobar\Deliverydate\Api\DeliveryRepositoryInterface
     */
    protected $deliveryRepository;

    /**
     * @var \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory
     */
    protected $deliveryFactory;

    protected $sortOrder;

    protected $layoutProcessors;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory $itemFactory,
        \Isobar\Bannerslider\Api\ItemRepositoryInterface $itemRepository,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrder $sortOrder,
        \Isobar\Deliverydate\Api\DeliveryRepositoryInterface $deliveryRepository,
        \Isobar\Deliverydate\Api\Data\DeliveryInterfaceFactory $deliveryFactory,
        array $layoutProcessors = [],
        array $data = []
    )
    {
        $this->itemFactory = $itemFactory;
        $this->itemRepository = $itemRepository;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrder = $sortOrder;
        $this->deliveryRepository = $deliveryRepository;
        $this->deliveryFactory = $deliveryFactory;
        parent::__construct($context, $data);
        $this->layoutProcessors = $layoutProcessors;
    }

    /**
     * @return string
     */
    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return parent::getJsLayout();
    }

    /**
     * Get item collection
     * @return \Isobar\Bannerslider\Api\Data\ItemSearchResultsInterface
     */
    public function getBannerCollection()
    {
        $filter = $this->filterBuilder
            ->create()
            ->setField(\Isobar\Bannerslider\Api\Data\ItemInterface::STATUS)
            ->setValue('1')
            ->setConditionType('eq');

        $filterGroup = $this->filterGroupBuilder
            ->addFilter($filter)
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder
            ->setFilterGroups([$filterGroup])
            ->create();
        $sortOrder = $this->sortOrder->setField('position')->setDirection(\Magento\Framework\Api\SortOrder::SORT_ASC);
        $searchCriteria->setSortOrders([$sortOrder]);
        $result = $this->itemRepository->getList($searchCriteria);
        return $result;
    }

}