define([
    'uiComponent',
    'ko'
], function (Component, ko) {
    'use strict';
    var listCustomers = ko.observableArray([]);
    return Component.extend({
        defaults: {
            template: 'Isobar_Bannerslider/banner'
        },
        hasCustomer: false,
        customers : [
            {'entity_id': 1, 'email': 'nhat@gmail.com', 'firstname': 'Nhat', 'lastname': 'Nguyen', 'created_at': '1/1/1990'},
            {'entity_id': 2, 'email': 'hao@gmail.com', 'firstname': 'Xuan', 'lastname': 'Hao', 'created_at': '2/2/1992'}
        ],
        initialize: function () {
            this._super();
            if (this.customers.length > 0) {
                this.hasCustomer = true;
            }
            return this;
        },
        rendertxt: function() {
            return 'Nhat  is here';
        },
        getListCustomers : function() {
            return this.customers;
        }

    });
});