define([
    'uiComponent',
    'ko'
], function (Component, ko) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'Isobar_Bannerslider/banner/child',
        },
        initialize: function () {
            this._super();
            return this;
        },
        renderTxtForExample: function() {
            return 'This is html of banner child';
        }
    });
});