define([
    'uiComponent',
    'ko'
], function (Component, ko) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'Isobar_Bannerslider/banner/child/child1',
        },
        initialize: function () {
            this._super();
            return this;
        },
        renderTxtForChild1: function() {
            return 'This is html of banner child1';
        }
    });
});