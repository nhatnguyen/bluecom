<?php
namespace Isobar\Bannerslider\Model\Item;

use Isobar\Bannerslider\Model\ResourceModel\Item\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Isobar\Bannerslider\Model\ResourceModel\Item\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->collection = $blockCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Isobar\Bannerslider\Model\Item $item */
        foreach ($items as $item) {
            $data = $item->getData();
            if (isset($item['img']) && $item['img']) {
                $elementPaths = explode('/', $item['img']);
                $data['image'][0]['name'] = end($elementPaths);
                $data['image'][0]['url'] = $this->_getImageUrl($item['img']);
            }
            $this->loadedData[$item->getId()] = $data;
        }

        $data = $this->dataPersistor->get('isobar_bannerslider');
        if (!empty($data)) {
            $item = $this->collection->getNewEmptyItem();
            $item->setData($data);
            $this->loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear('isobar_bannerslider');
        }

        return $this->loadedData;
    }

    /**
     * Get Image Url
     * @param $imgPath
     * @return string
     */
    protected function _getImageUrl($imgPath)
    {
        return $this->storeManager->getStore()->getBaseUrl().$imgPath;
    }
}
