<?php

namespace Isobar\Bannerslider\Model\ResourceModel\Item;


class Collection extends \Isobar\Bannerslider\Model\ResourceModel\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * _contruct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Bannerslider\Model\Item', 'Isobar\Bannerslider\Model\ResourceModel\Item');
    }
}