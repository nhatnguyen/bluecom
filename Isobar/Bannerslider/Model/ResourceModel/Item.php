<?php

namespace Isobar\Bannerslider\Model\ResourceModel;


class Item extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('isobar_bannerslider', 'id');
    }
}