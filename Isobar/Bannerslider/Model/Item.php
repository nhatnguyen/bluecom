<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\Bannerslider\Model;


class Item extends \Magento\Framework\Model\AbstractModel implements \Isobar\Bannerslider\Api\Data\ItemInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Bannerslider\Model\ResourceModel\Item');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        return $this->setData('title', $title);
    }

    /**
     * {@inheritdoc}
     */
    public function getImg()
    {
        return $this->_getData(self::IMG);
    }

    /**
     * {@inheritdoc}
     */
    public function setImg($img)
    {
        return $this->setData('img', $img);
    }

    /**
     * {@inheritdoc}
     */
    public function getImgLink()
    {
        return $this->_getData(self::IMG_LINK);
    }

    /**
     * {@inheritdoc}
     */
    public function setImgLink($imgLink)
    {
        return $this->setData('img_link', $imgLink);
    }

    /**
     * {@inheritdoc}
     */
    public function getAlt()
    {
        return $this->_getData(self::ALT);
    }

    /**
     * {@inheritdoc}
     */
    public function setAlt($alt)
    {
        return $this->setData('alt', $alt);
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return $this->_getData(self::POSITION);
    }

    /**
     * {@inheritdoc}
     */
    public function setPosition($position)
    {
        return $this->setData('position', $position);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData('status', $status);
    }
}