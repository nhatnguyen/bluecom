<?php
namespace Isobar\Bannerslider\Controller\Adminhtml\Item;
use Isobar\Bannerslider\Model\Item;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Magento\Framework\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory
     */
    protected $itemFactory;

    /**
     * @var \Isobar\Bannerslider\Api\ItemRepositoryInterface
     */
    protected $itemRepository;

    /**
     * @var \Isobar\Bannerslider\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\File\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Filesystem $filesystem
     * @param DataPersistorInterface $dataPersistor
     * @param \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory $itemFactory
     * @param \Isobar\Bannerslider\Api\ItemRepositoryInterface $itemRepository
     * @param \Isobar\Bannerslider\Model\ImageUploader $imageUploader
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Filesystem $filesystem,
        DataPersistorInterface $dataPersistor,
        \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory $itemFactory,
        \Isobar\Bannerslider\Api\ItemRepositoryInterface $itemRepository,
        \Isobar\Bannerslider\Model\ImageUploader $imageUploader,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->uploaderFactory = $uploaderFactory;
        $this->itemFactory = $itemFactory;
        $this->itemRepository = $itemRepository;
        $this->dataPersistor = $dataPersistor;
        $this->imageUploader = $imageUploader;
        $this->storeManager = $storeManager;
        parent::__construct($context, $coreRegistry);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Item::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }

            try {
                $item = $this->itemFactory->create();
                if (!isset($data['image'])) {
                    $data['img'] = '';
                }
                if (isset($data['image'][0]['name']) && isset($data['image'][0]['path'])) {
                    if (file_exists($data['image'][0]['path'].'/'.$data['image'][0]['name'])) {
                        $basePath = $this->imageUploader->getBasePath();
                        $imgName = $this->imageUploader->moveFileFromTmp($data['image'][0]['name']);
                        $filePath = $this->imageUploader->getFilePath($basePath, $imgName);
                        $data['img'] = '/media/' . $filePath;
                    }
                }
                $item->setData($data);
                $model = $this->itemRepository->save($item);
                if (!$model->getId() && $id) {
                    $this->messageManager->addError(__('This banner no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
                $this->messageManager->addSuccess(__('You saved the banner.'));
                $this->dataPersistor->clear('isobar_bannerslider');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                echo $e->getMessage();die();
                $this->messageManager->addException($e, __('Something went wrong while saving the banner.'));
            }

            $this->dataPersistor->set('isobar_bannerslider', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
