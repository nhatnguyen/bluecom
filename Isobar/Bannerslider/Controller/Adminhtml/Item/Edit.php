<?php
namespace Isobar\Bannerslider\Controller\Adminhtml\Item;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Bannerslider\Api\ItemRepositoryInterface
     */
    protected $itemReposity;

    /**
     * @var \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory
     */
    protected $itemFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Bannerslider\Api\ItemRepositoryInterface $itemReposity,
        \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory $itemFactory
    ) {
        $this->itemReposity = $itemReposity;
        $this->itemFactory = $itemFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit Isobar Banner
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = $this->itemFactory->create();

        // 2. Initial checking
        if ($id) {
            $model = $this->itemReposity->get($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This banner no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('slider_banner', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Bannerslider::bannerslider')->addBreadcrumb(__('Banner'), __('Banner'))
            ->addBreadcrumb(
            $id ? __('Edit Block') : __('New Banner'),
            $id ? __('Edit Block') : __('New Banner')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Banner'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Banner'));
        return $resultPage;
    }
}
