<?php
namespace Isobar\Bannerslider\Controller\Adminhtml\Item;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory
     */
    protected $itemFactory;

    /**
     * @var \Isobar\Bannerslider\Api\ItemRepositoryInterface
     */
    protected $itemReposity;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Bannerslider\Api\Data\ItemInterfaceFactory $itemFactory,
        \Isobar\Bannerslider\Api\ItemRepositoryInterface $itemReposity
    ) {
        $this->itemFactory = $itemFactory;
        $this->itemReposity = $itemReposity;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Bannerslider::bannerslider')->addBreadcrumb(__('Banner'), __('Banner'));
        $resultPage->getConfig()->getTitle()->prepend(__('Banner Management'));
        return $resultPage;
    }
}
