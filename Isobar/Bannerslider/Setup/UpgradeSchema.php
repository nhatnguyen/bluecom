<?php

namespace Isobar\Bannerslider\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $tableName = $setup->getTable('isobar_bannerslider');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $columns = array(
                    'alt' => array(
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        ['nullable' => true, 'default' => ''],
                        'comment' => 'ALT Image',
                    ),
                );
                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
        $setup->endSetup();
    }
}